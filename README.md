# EZNEC Antennas

A collection of antennas modeled with EZNEC. Some have been built and tested, some have not.
If anyone would like dimensions on these antennas open an issue and let me know.

## Descriptions:

## 2.4ghz double-biquad
###### model not yet complete
![wifi double biquad](doublebiquad.png)

An attempt at modeling a 2.4Ghz double-biquad for extending wifi coverage. Aside from the SWR mismatch I'm pretty sure the farfield plot should not show a central null, but haven't figured out what's wrong with the wireframe yet.

## 2 meter moxen rectangle
###### not yet built
![2 meter moxen antenna](2mMoxonRectangle.png)

A directional antenna for the 2 meter band. Fairly wide coverage but with a great front to back ratio. About 8db gain, and SWR below 2 for the whole band. I'm curious as to why the rear lobe increases with a lower frequency, yet decreases with frequencies above the minimum SWR. Horizontal polarization shown in picture.

## 433mhz Moxen rectangle
###### not yet built
![433mhz moxen antenna](433moxensweep.png)

Same as above, but scaled for 70cm band. Original idea was to improve range on cheap 433Mhz arduino modules. Good SWR for most of the band, hard to get it below 2 for the entire band

## ADSB spider
###### Built, tested and installed
![adsb spider antenna](adsbspider.png)

Simple 1/2 wave ground plane for receiving ADSB traffic data. Tested with and without a preamp. The null is real: air traffic flying right overhead looses reception until it's about a mile away from the station again. Built by soldering brass brazing wire to so-239 panel mount connector.

## 2 meter aluminum J-pole
###### Built, tested, awaiting install
![aluminum j-pole 146mhz](AluAngleJ-2meter_v2.png)

Strong and slim j-pole for 2 meters. Built by threading the bottom inch of 3/8 aluminum round stock, and bolting it to an aluminum angle base. Coax is crimped to ring terminals and bolted to the verticals. SWR is < 2 for the whole band. Wire diameter for model is set at 0.2inches: nec2 sometimes does strange things with close spacing of wide-diameter elements. Something about a diameter:wavelength ratio issue.

## VHF biconical receiver
###### Built, tested and installed
![biconical antenna](biconicalVHF.png)

A good scanner antenna. Frequency is centered on the air band(118-136Mhz) but received local weather, police and fire traffic just fine.(~170Mhz)

## Suburban roof
###### Handy template
![wireframe of truck roof](suburbanwireframe.png)

Simple wireframe model of a suburban roof. Add your own antennas to get an idea of what their pattern might look like. Fairly omnidirectional if centered (see VHF example below). You can add sides and model antennas mounted near the side of the hood. It's as bad as you'd expect (antenna pattern quarters away from car, terrible omnidirectional coverage)

![wireframe roof with vhf antenna on top](suburbanroof2meter.png)
